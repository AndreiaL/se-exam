package Aplicatia_1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class UserInterface extends JFrame {
    JTextField text_name, text_content;
    JButton button;

    UserInterface(){
        setTitle("Write in the text file");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 400);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        text_name = new JTextField();
        text_name.setBounds(40,50,200, 20);

        text_content = new JTextField();
        text_content.setBounds(40,80,200, 20);

        button = new JButton("Write");
        button.setBounds(150,140,100, 30);

        button.addActionListener(new PressButton());

        add(text_name); add(text_content); add(button);
    }
    class PressButton implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                FileWriter myWriter = new FileWriter("C://Users/Andreia/Desktop/"+text_name.getText());
                myWriter.write(text_content.getText());
                myWriter.close();

            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
        new UserInterface();
    }
}
