package Aplicatia_2;

import java.util.AbstractCollection;

public class User {
    public Account[] accounts;

    User(){ //It does not matter how many Accounts I have as it is a 0 or many relationship
        accounts[0] = new Account();
        accounts[1] = new Account();
        accounts[2] = new Account();
    }
}

class Bank{
    public User[] users;
    public Account[] accounts;

    Bank(){

    }
}

class Account {
    Card card;

    Account(){
        card = new Card();
    }
}

class Transaction{
    private int amount;
    Account[] accounts = new Account[2];

    Transaction(){
        accounts[0] = new Account();
        accounts[1] = new Account();
    }
}

class Card{

}