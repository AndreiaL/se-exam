public interface Interface1 {
}

class A implements Interface1{
    private M m;
    public void metA(){
        this.m = new M();
    }
}

class M{
    private B b;
    M(){

    }

    M(B b){
        b = new B();
    }
}

class B{
    public void metB(){

    }
}

class X{
    public void i(L l){
        l.f();
    }
}

class L{
    private long t;
    private M m;
    public void f(){

    }

    L(){
        m = new M();
    }
}
