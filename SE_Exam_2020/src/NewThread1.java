import javax.swing.plaf.TableHeaderUI;
import java.util.List;

public class NewThread1 extends Thread{

    NewThread1(){

    }
    NewThread1(String name){
        super(name);
    }

    public void run(){
        synchronized (Main.monitor) {
            for (int i = 0; i < 10; i++) {
                System.out.println(getName() + " - " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class NewThread2 extends Thread{

    NewThread2(){

    }

    NewThread2(String name){
        super(name);

    }

    public void run() {
        synchronized (Main.monitor) {
            for (int i = 30; i < 40; i++) {
                System.out.println(getName() + " - " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

class NewThread3 extends Thread{

    NewThread3(){

    }

    NewThread3(String name){
        super(name);

    }

    public void run() {
        synchronized (Main.monitor) {
            for (int i = 10; i < 20; i++) {
                System.out.println(getName() + " - " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}

class NewThread4 extends Thread{
    NewThread4(){

    }

    NewThread4(String name){
        super(name);

    }

    public void run() {
        synchronized (Main.monitor) {
            for (int i = 20; i < 30; i++) {
                System.out.println(getName() + " - " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class Main {
    static final Object monitor = new Object();
    public static void main(String[] args) {
        NewThread1 t1 = new NewThread1("NewThread1");
        NewThread2 t2 = new NewThread2("NewThread2");
        NewThread3 t3 = new NewThread3("NewThread3");
        NewThread4 t4 = new NewThread4("NewThread4");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}